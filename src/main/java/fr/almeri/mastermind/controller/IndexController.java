package fr.almeri.mastermind.controller;

import fr.almeri.mastermind.model.Color;
import fr.almeri.mastermind.model.Mastermind;
import fr.almeri.mastermind.model.Solution;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class IndexController {

    //Solution du jeu : combinaison à trouver
    private Solution solution;

    //On intercepte l'URL / envoyée en GET
    @GetMapping("/")
    public String index(Model pModel){
        //Lorsque j'appelle localhost:8081/ , j'exécute le contenu de la méthode index()
        //On passe à la vue l'objet nbTests qui contient la valeur 12
        pModel.addAttribute("nbTests", 12);

        //TypeObjet nomObjet = new Contructeur();
        //Je créé une solution qui contient 4 billes : une bleue, deux blanches, une rouge
        solution = new Solution(Color.BLEU,
                Color.JAUNE, Color.JAUNE, Color.ROUGE);
        //On passe la solution créée en JAVA à la page HTML
        pModel.addAttribute("solution", solution);

        //Je récupère la liste des couleurs & je la passe à l'HTML
        Color[] colors = Color.getPions();
        pModel.addAttribute("colors", colors);

        //Retourne la page HTML qui s'appelle index dans le répertoire templates
        return "index";
    }

    //On intercepte l'URL /testSolution qui contient dans son body (@RequestBody)
    // un objet de type Solution qui correspond à la proposition de l'utilisateur
    @PostMapping("/testSolution")
    @ResponseBody
    public String testSolution(Model pModel, @RequestBody Solution pProposition){
        System.out.println("Solution à trouver");
        System.out.println(solution);
        System.out.println("Proposition de l'utilisateur");
        System.out.println(pProposition);
        if(solution.equals(pProposition)){
           //Le joueur a gagné
            return "{\"end\" : \"Vous avez gagné !\"}";
        }
        else {
            //Vérifier les pions pour lui indiquer bonne ou mauvaise place
            //Deux tableaux
            //On vérifie tous les éléments qui sont corrects
            Solution response = new Solution();

            List<String> solutionList = new ArrayList<>();
            solutionList.add(solution.getBall1());
            solutionList.add(solution.getBall2());
            solutionList.add(solution.getBall3());
            solutionList.add(solution.getBall4());

            List<String> propositionList = new ArrayList<>();
            propositionList.add(pProposition.getBall1());
            propositionList.add(pProposition.getBall2());
            propositionList.add(pProposition.getBall3());
            propositionList.add(pProposition.getBall4());

            //ALGORITHME A REVOIR
            //Test bonnes places
            for(int i=0; i<propositionList.size() ; i++) {
                if (propositionList.get(i).equals(solutionList.get(i))) {
                    response.addResponse(i, Color.ROUGE.getRgbColor());
                    solutionList.set(i, Color.NO_COLOR.getRgbColor());
                }
            }
            //Test mauvaises places
            for(int i=0; i<propositionList.size() ; i++){
                if(response.getColor(i) != null){
                    continue;
                }

                boolean find = false;
                for(int j = 0; j < solutionList.size(); j++){
                    if(propositionList.get(i).equals(solutionList.get(j))){
                        response.addResponse(i, Color.BLANC.getRgbColor());
                        solutionList.set(j, Color.NO_COLOR.getRgbColor());
                        find = true;
                        break;
                    }
                }
                if(!find) {
                    response.addResponse(i, Color.NO_COLOR.getRgbColor());
                }
            }

            System.out.println(response.toString());
            //Je renvoie la réponse avec les billes rouges, blanches et transparentes
            return response.toString();
        }
    }




    /*
     //Tant que propositionList n'est pas vide, on compare
            while(counter < propositionList.size()){
                //Je parcours toutes les propositions
                for(int i=0; i<propositionList.size(); i++){
                    //Si prop[i] == solution[i] alors la bille est correcte, je l'ajoute à response
                    //& je supprime la proposition de la liste
                    if(propositionList.get(i).equals(solutionList.get(i))){
                        //J'ajoute une bille noire dans la solution
                        response = addResponse(counter, Color.NOIR.getRgbColor(), response);
                        //je remplace la proposition de la liste ainsi que la solution utilisée par un pion sans couleur
                        propositionList.set(i, Color.NO_COLOR.getRgbColor());
                        solutionList.set(i, Color.NO_COLOR.getRgbColor());
                        counter++;
                    }
                    else{
                        //Sinon par défaut blanc
                        boolean find = false;
                        for(int j=0; j<solutionList.size(); j++){
                            if (propositionList.get(i).equals(solutionList.get(j))) {
                                find = true;

                                //je remplace la proposition de la liste ainsi que la solution utilisée par un pion sans couleur
                                propositionList.set(i, Color.NO_COLOR.getRgbColor());
                                solutionList.set(j, Color.NO_COLOR.getRgbColor());
                                break;
                            }
                        }
                        //Je parcours toutes les solutions. Si la proposition est dans la solution, mauvaise place. Si pas trouvé sans couleur
                        if(find){
                            response = addResponse(counter, Color.BLANC.getRgbColor(), response);
                        }
                        else{
                            response = addResponse(counter, Color.NO_COLOR.getRgbColor(), response);
                            //Je remplace la proposition de la liste par un pion sans couleur
                            //Puisqu'aucun pion de la solution n'a été utilisé, on n'y touche pas
                            propositionList.set(i, Color.NO_COLOR.getRgbColor());
                        }
                        counter++;
                    }
                }
            }
     */
}
