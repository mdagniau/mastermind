package fr.almeri.mastermind.model;

import java.util.List;

public class Mastermind {

    private Solution solution;
    private int noTest;
    private int nbTests = 12;
    private List<String> colorInBalls;

    public Mastermind(){

    }

    public Mastermind(Solution pSolution){
        this.solution = pSolution;
    }

    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution pSolution) {
        this.solution = pSolution;
    }

    public int getNoTest() {
        return noTest;
    }

    public void setNoTest(int pNoTest) {
        this.noTest = pNoTest;
    }

    public int getNbTests() {
        return nbTests;
    }

    public void setNbTests(int pNbTests) {
        this.nbTests = pNbTests;
    }

    public List<String> getColorInBalls() {
        return colorInBalls;
    }

    public void setColorInBalls(List<String> pColorInBalls) {
        this.colorInBalls = pColorInBalls;
    }
}
