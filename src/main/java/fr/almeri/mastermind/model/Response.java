package fr.almeri.mastermind.model;

public enum Response {
    CORRECT, INCORRECT, BAD_PLACE;
}
