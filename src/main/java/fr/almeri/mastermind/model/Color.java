package fr.almeri.mastermind.model;

public enum Color {

    JAUNE("rgb(255, 255, 0)"),
    BLEU("rgb(0, 0, 255)"),
    VERT("rgb(0, 128, 0)"),
    ROUGE("rgb(255, 0, 0)"),
    BLANC("rgb(255, 255, 255)"),
    GRIS("rgb(211, 211, 211)"),
    NO_COLOR("transparent"),
    NOIR("rgb(0, 0, 0)");

    private final String rgbColor;

    Color(String pRgbColor){
        this.rgbColor = pRgbColor;
    }

    public String getRgbColor(){
        return this.rgbColor;
    }

    /**
     * Méthode ne retournant que les couleurs nécessaires pour jouer
     * Color.BLANC et Color.NOIR servent pour les réponses
     * @return
     */
    public static Color[] getPions(){
        Color[] colors = {Color.BLEU, Color.ROUGE, Color.VERT, Color.JAUNE};
        return colors;
    }
}
