package fr.almeri.mastermind.model;

import java.util.Objects;

public class Solution {

    private String ball1;
    private String ball2;
    private String ball3;
    private String ball4;

    public Solution(){

    }

    public Solution(String pBall1, String pBall2,
                    String pBall3, String pBall4){
        this.ball1 = pBall1;
        this.ball2 = pBall2;
        this.ball3 = pBall3;
        this.ball4 = pBall4;
    }

    /**
     * Initialise solution with color
     * getRgbColor() for String information
     * @param pColor1
     * @param pColor2
     * @param pColor3
     * @param pColor4
     */
    public Solution(Color pColor1, Color pColor2, Color pColor3, Color pColor4){
        this.ball1 = pColor1.getRgbColor();
        this.ball2 = pColor2.getRgbColor();
        this.ball3 = pColor3.getRgbColor();
        this.ball4 = pColor4.getRgbColor();
    }

    public String getBall1() {
        return this.ball1;
    }

    public void setBall1(String pBall1) {
        this.ball1 = pBall1;
    }

    public String getBall2() {
        return this.ball2;
    }

    public void setBall2(String pBall2) {
        this.ball2 = pBall2;
    }

    public String getBall3() {
        return this.ball3;
    }

    public void setBall3(String pBall3) {
        this.ball3 = pBall3;
    }

    public String getBall4() {
        return this.ball4;
    }

    public void setBall4(String pBall4) {
        this.ball4 = pBall4;
    }

    public Solution addResponse(int i, String rgbColor) {
        //En fonction du compteur, je mets la réponse dans telle ou telle bille
        switch(i){
            case 0:
                this.setBall1(rgbColor);
                break;
            case 1:
                this.setBall2(rgbColor);
                break;
            case 2:
                this.setBall3(rgbColor);
                break;
            default:
                this.setBall4(rgbColor);
        }
        return this;
    }

    public String getColor(int i){
        switch(i){
            case 0:
                return ball1;
            case 1:
                return ball2;
            case 2:
                return ball3;
            default:
                return ball4;
        }
    }

    @Override
    public String toString() {
        return "{" +
                "\"ball1\":\"" + ball1 + "\"" +
                ", \"ball2\":\"" + ball2 + "\"" +
                ", \"ball3\":\"" + ball3 + "\"" +
                ", \"ball4\":\"" + ball4 + "\"" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Solution solution = (Solution) o;
        return Objects.equals(ball1, solution.ball1)
                && Objects.equals(ball2, solution.ball2)
                && Objects.equals(ball3, solution.ball3)
                && Objects.equals(ball4, solution.ball4);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ball1, ball2, ball3, ball4);
    }
}
